from ping_pong_functions import *
import time
import os
import sys
import pygame
import random

dimensions = (512, 512)
display: pygame.display = pygame.display.set_mode((dimensions[0], dimensions[1]))

Score = 0
pygame.font.init()
GAME_FONT = pygame.font.SysFont('Comic Sans MS', 30)
speeds = [-5, -4, -3, -2, 2, 3, 4, 5, 6, -6]
# color
game_color = "#AAAAAA"
# background
display.fill(pygame.Color(game_color))
white = (255, 255, 255)
red = (225, 0, 0)
width = 25
height = 150
velocity_x = random.choice(speeds)
velocity_y = random.choice(speeds)
movement = 15
radius = 8

# TODO turn PLAYERS TO PONG CLASS
# TODO TURN ball into BALL CLASS
# TODO ADD MASKS FOR PIXEL PERFECT COLLISION

human: pygame.Rect = pygame.draw.rect(display, white, (width, dimensions[0] // 2 + height // 2, -width, -height))
opponent: pygame.Rect = pygame.draw.rect(display, white,
                                         (dimensions[0] - width, dimensions[0] // 2 + height // 2, width, -height))
ball: pygame.Rect = pygame.draw.circle(display, red, (dimensions[0] // 2, dimensions[0] // 2), radius, radius)
# TODO look into collision fucntions!
# TODO movement, trajectories , listening to key holddown!, AI movement!
clock = pygame.time.Clock()


def draw_objects():
    display.fill(pygame.Color(game_color))
    pygame.draw.rect(display, white, human)
    pygame.draw.rect(display, white, opponent)
    pygame.draw.circle(display, red, (ball.x, ball.y), radius, radius)


def checkWinCond():
    if ball.x <= 0 or ball.x >= dimensions[0]:
        return True
    return False


def make_opponent_move():  # very simple AI
    if ball.x > dimensions[0] // 3 and velocity_x < 0:  # if the ball is heading towards you
        if opponent.y < dimensions[0] - opponent.height and ball.y - opponent.y > 0:
            # if you can move down and you should
            opponent.y += 1
        else:  # else just go up if you can
            if ball.y - opponent.y < 0 and opponent.y > 0:
                opponent.y -= 1


def main():
    global velocity_x, velocity_y, Score
    while True:
        cond = checkWinCond()
        for _ in range(Score + 1):
            make_opponent_move()
        if cond:
            if ball.x <= 0:
                Score = 0
            else:
                Score += 1
            velocity_x = random.choice(speeds)
            velocity_y = random.choice(speeds)
            ball.x, ball.y = dimensions[0] // 2, dimensions[0] // 2

        # dev if- stay inside square:
        if ball.x > dimensions[1] or ball.x < 0:
            velocity_x *= -1

        if human.colliderect(ball) or opponent.colliderect(ball):
            if velocity_x > 0:
                velocity_x += 1
            else:
                velocity_x -= 1
            print(human)
            print(opponent)
            print(ball)
            print("boom")
            velocity_x *= -1
        if ball.y - radius < 0 or ball.y + radius > dimensions[1]:
            velocity_y *= -1
        # get all keys, so holding the key counts
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_UP] and human.y > 0:
            # print("up pressed")
            human.y -= movement
        if pressed_keys[pygame.K_DOWN] and human.y < dimensions[1] - human.height:
            human.y += movement

        # balls always moves!
        ball.x -= velocity_x
        ball.y -= velocity_y
        # Gotta go fast

        # increase score
        text_surface = GAME_FONT.render(f'Score: {Score}', True, (25, 25, 25))
        display.blit(text_surface, (dimensions[0] // 2.5, 0))
        pygame.display.flip()
        clock.tick(40)
        draw_objects()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()


if __name__ == "__main__":
    # starting positions
    main()
